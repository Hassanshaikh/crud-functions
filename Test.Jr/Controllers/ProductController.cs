﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test.Jr.Implement;
using Test.Jr.Interface;
using Test.Jr.Models;

namespace Test.Jr.Controllers
{
    public class ProductController : Controller
    {
        IProduct _ProductServices = new BLProduct();
        // GET: Product
        public ActionResult List()
        {
            var product = _ProductServices.ProductList();
            return View(product);
        }
        [HttpGet]
        public ActionResult Add()
        {
            ViewBag.message = "";
            return View();
        }
        [HttpPost]
        public ActionResult Add(Product product)
        {
            var Add = _ProductServices.ProductList();
            _ProductServices.Add(product);
            return RedirectToAction("List");
        }
        [HttpGet]
        public ActionResult Update()
        {
            ViewBag.message = "";
            return View();
        }
        [HttpPost]
        public ActionResult Update(Product product)
        {
            var update = _ProductServices.ProductList();
            _ProductServices.Update(product);
            return RedirectToAction("List");
        }
        [HttpPost]
        public ActionResult Delete(Product product) 
        {
            var delete = _ProductServices.ProductList();
            _ProductServices.Delete(product);
            return RedirectToAction("List");
        }
    }
}