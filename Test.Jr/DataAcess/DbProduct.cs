﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Test.Jr.Models;

namespace Test.Jr.DataAcess
{
    public class DbProduct
    {
        private static readonly string _ConnectionString = ConfigurationManager.ConnectionStrings["SqlCon"].ConnectionString;

        public List<Product> ProductList()
        {
            List<Product> productlist = new List<Product>();
            using (SqlConnection con = new SqlConnection(_ConnectionString))
            {
                string query = "SELECT * FROM Product";
                var cmd = new SqlCommand(query, con);
                con.Open();
                var dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    var product = new Product();
                    product.Id = Convert.ToInt32(dataReader["Id"]);
                    product.Name = Convert.ToString(dataReader["Name"]);
                    product.Description = Convert.ToString(dataReader["Description"]);
                    product.Created = Convert.ToDateTime(dataReader["Created"]);
                    
                    productlist.Add(product);
                }
                con.Close();
            }

            return productlist;
        }
        public string Add(Product product)
        {
            using (SqlConnection con = new SqlConnection(_ConnectionString))
            {
                string query = "INSERT INTO Product (Name,Description) VALUES ('" + product.Name + "','" + product.Description + "')";
                var cmd = new SqlCommand(query, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

            }
            return "Added";
        }
        public string Update(Product product)
        {
            using (SqlConnection con = new SqlConnection(_ConnectionString))
            {
                string query = "UPDATE Product SET Name ='" + product.Name + "',Description ='" + product.Description + "' WHERE Id= '" + product.Id + "'";
                var cmd = new SqlCommand(query, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            return "Updated";
        }
        public string Delete(Product product)
        {
            using (SqlConnection con = new SqlConnection(_ConnectionString))
            {
                string query = "";
                var cmd = new SqlCommand (query, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            return "Delete";
        }
    }
}