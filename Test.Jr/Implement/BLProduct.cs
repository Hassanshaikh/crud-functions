﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Jr.Interface;
using Test.Jr.DataAcess;
using System.Runtime.InteropServices;
using Test.Jr.Models;

namespace Test.Jr.Implement
{
    public class BLProduct:IProduct
    {
        DbProduct _db = new DbProduct();
         public List<Product> ProductList()
            {
            return _db.ProductList();
        }
        public string Add(Product product)
        {
            return _db.Add(product);

        }
        public string Update(Product product) 
        {
            return _db.Update(product);
        }
        public string Delete(Product product) 
        {
            return _db.Delete(product);
        }
    }
}