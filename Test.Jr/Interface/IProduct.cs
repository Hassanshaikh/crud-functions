﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Jr.Models;

namespace Test.Jr.Interface
{
    public interface IProduct
    {
        List<Product> ProductList();
        string Add(Product product);
        string Update(Product product);
        string Delete(Product product);

    }
}